# 3088 PiHat Project

This project is on making a microHat that allows the Pi Zero to continue working when there is
a power outage and to gracefully shutdown running programs if there is a power outage.

Bill of Materials for Battery Status LEDs circuitry:  
100kOhm resistor, 
10kOhm resistor, 
two 330Ohm resistors, 
two NPN BJTs, 
zener diode with a reverse knee of 6,9V 
and two LEDs

Bill of Materials for the Power Status LED circuitry: 
750Ohm resistor, 
and white LED with a voltage drop of 3,5V

